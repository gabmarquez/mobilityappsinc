package com.gabrielamarquez.mobilityapp.employees;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gabrielamarquez.mobilityapp.R;
import com.gabrielamarquez.mobilityapp.adapter.EmployeesAdapter;
import com.gabrielamarquez.mobilityapp.database.Database;
import com.gabrielamarquez.mobilityapp.model.Employees;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class ListEmployees extends Activity {

    private ListView listView;
    private EmployeesAdapter adapter;
    private List<Employees> employeesList;
    private Database database;

    private Button btnAdd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_employees);

        listView = (ListView)findViewById(R.id.listview_emp);
        database = new Database(this);
        btnAdd = (Button)findViewById(R.id.btnAdd);

        File database = getApplicationContext().getDatabasePath(Database.DBNAME);
        if(false == database.exists()) {
            this.database.getReadableDatabase();
            //Copy db
            if(copyDatabase(this)) {
                Toast.makeText(this, "Copy database succes", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Copy data error", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        employeesList = this.database.getListEmployees();

        adapter = new EmployeesAdapter(this, employeesList);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int idEmp = (int) view.getTag();
                Toast.makeText(getApplicationContext(), idEmp + "", Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(), ((TextView) view.findViewById(R.id.nameEmp)).getText().toString(), Toast.LENGTH_SHORT).show();

            }
        });
        registerForContextMenu(listView
        );
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), EmployeesActivity.class);
                i.putExtra("Action", "Add");
                startActivity(i);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        employeesList = database.getListEmployees();
        adapter.updateList(employeesList);
    }

    private boolean copyDatabase(Context context) {
        try {

            InputStream inputStream = context.getAssets().open(Database.DBNAME);
            String outFileName = Database.DBLOCATION + Database.DBNAME;
            OutputStream outputStream = new FileOutputStream(outFileName);
            byte[]buff = new byte[1024];
            int length = 0;
            while ((length = inputStream.read(buff)) > 0) {
                outputStream.write(buff, 0, length);
            }
            outputStream.flush();
            outputStream.close();
            Log.w("MainActivity","DB copied");
            return true;
        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        View v = (View) listView.getAdapter().getView(
                info.position, null, null);

        switch (item.getItemId()) {
            case R.id.menu_item_edit:

                Intent i = new Intent(getApplicationContext(), EmployeesActivity.class);
                i.putExtra("Action", "Edit");
                i.putExtra("Id", (int)v.getTag());
                startActivity(i);
                break;

            case R.id.menu_item_del:
                if( database.deleteEmployeeById((int)v.getTag())){
                    Toast.makeText(getApplicationContext(),"Emilinado", Toast.LENGTH_SHORT).show();
                    employeesList.remove(info.position);
                    adapter.updateList(employeesList);
                } else {
                    Toast.makeText(getApplicationContext(),"fallo", Toast.LENGTH_SHORT).show();
                }

                break;
        }
        return true;
    }
}
