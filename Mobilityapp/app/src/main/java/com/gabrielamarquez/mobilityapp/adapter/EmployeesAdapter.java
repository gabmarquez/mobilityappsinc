package com.gabrielamarquez.mobilityapp.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gabrielamarquez.mobilityapp.R;
import com.gabrielamarquez.mobilityapp.model.Employees;

import java.util.List;

public class EmployeesAdapter extends BaseAdapter {
    private Context context;
    private List<Employees> employeesList;

    public EmployeesAdapter(Context context, List<Employees> employeesList) {
        this.context = context;
        this.employeesList = employeesList;
    }

    @Override
    public int getCount() {
        return employeesList.size();
    }

    @Override
    public Object getItem(int position) {
        return employeesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return employeesList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = View.inflate(context, R.layout.item_employees, null);
        TextView nameEmp = (TextView)v.findViewById(R.id.nameEmp);
        TextView birthdate = (TextView)v.findViewById(R.id.birthdate);
        TextView jobEmp = (TextView)v.findViewById(R.id.jobEmp);

        nameEmp.setText(employeesList.get(position).getNombre());
        birthdate.setText(employeesList.get(position).getFechaNacimiento());
        jobEmp.setText(employeesList.get(position).getPuesto());

        v.setTag( employeesList.get(position).getId());

        return v;
    }

    public void updateList(List<Employees> lstItem) {
        employeesList.clear();
        employeesList.addAll(lstItem);
        this.notifyDataSetChanged();
    }
}
