package com.gabrielamarquez.mobilityapp.maps;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.gabrielamarquez.mobilityapp.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private EditText lat, lon;
    private Button button;
    GoogleMap googleMap;
    Marker marker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        lat = (EditText) findViewById(R.id.lat);
        lon = (EditText) findViewById(R.id.lon);

        button = (Button)findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view)  {

                // Validacion
                if(lat.getText().toString().isEmpty()) {
                    Toast.makeText(getBaseContext(), "Ingresa latitud", Toast.LENGTH_SHORT).show();
                }
                // Validacion
                if(lon.getText().toString().isEmpty()) {
                    Toast.makeText(getBaseContext(), "Ingresa longitud", Toast.LENGTH_SHORT).show();
                }

                Log.v("EditText", lat.getText().toString());
                Log.v("EditText", lon.getText().toString());

                String  latitude = lat.getText().toString();
                String longitude = lon.getText().toString();

                if (lat.getText().toString().isEmpty() && lon.getText().toString().isEmpty()){
                    Toast.makeText(MapsActivity.this, "No se ha encontrado ningun marker", Toast.LENGTH_LONG).show();
                } else {

                    LatLng latLng = new LatLng(Double.parseDouble(latitude),Double.parseDouble(longitude));

                    googleMap.addMarker(new MarkerOptions().position(latLng).title("Nuevo marker"));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                }

            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

    }


}