package com.gabrielamarquez.mobilityapp;

public interface OnTaskCompleted {
    void onTaskCompleted(String finish);
}
