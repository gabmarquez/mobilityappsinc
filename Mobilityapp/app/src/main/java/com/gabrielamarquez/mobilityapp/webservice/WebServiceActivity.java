package com.gabrielamarquez.mobilityapp.webservice;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gabrielamarquez.mobilityapp.R;
import com.gabrielamarquez.mobilityapp.model.Place;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.ankit.jarextraction.Extract;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

public class WebServiceActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final String TAG = "PERMISSION";
    GoogleMap googleMap;
    ArrayList<Place> places;
    LatLng defaultLatLng = new LatLng(13.714966, -89.155755);

    private final int REQUEST_PERMISSION_WES = 1;

    FollowPosition followPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_service);

        showPhoneStatePermission();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

//        followPosition = new FollowPosition(this.googleMap, WebServiceActivity.this);
//        followPosition.register(WebServiceActivity.this);
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(defaultLatLng));
        chooseMoveCamera(googleMap, defaultLatLng, 10);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, new IntentFilter(FetchPlacesService.NOTIFICATION));

        Intent intent = new Intent(this, FetchPlacesService.class);
        startService(intent);
        if (followPosition != null) {
            followPosition.register(WebServiceActivity.this);
        }
    }

    @Override
    protected void onPause() {
        unregisterReceiver(broadcastReceiver);
        if (followPosition != null)
            followPosition.unRegister(WebServiceActivity.this);
        super.onPause();
    }

    private void chooseMoveCamera(GoogleMap googleMap, LatLng tmpLatLng, int zoom){
        CameraPosition cameraPosition = new CameraPosition.Builder().zoom(zoom).target(tmpLatLng).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("SALIDA","Receiver se ejecuta inicio");
            Bundle bundle = intent.getExtras();

            if (bundle != null) {

                places = (ArrayList<Place>)
                        bundle.getSerializable(FetchPlacesService.RESULT);

                if (places != null && places.size() > 0) {
                    if (googleMap != null) {
                        for (Place tmp : places) {

                            //LatLng tmpLatLng = new LatLng(tmp.getLat(), tmp.getLon());
                            //googleMap.addMarker(new MarkerOptions().position(tmpLatLng).title(tmp.getPlaceName()).snippet("id="+tmp.getId()));
                        }
                    }
                }
            }
        }
    };

    private void getFile() {

        String url = "http://74.205.41.248:8081/pruebawebservice/api/mob_sp_GetArchivo";
        RequestQueue queue = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("RESPONSE", "That work!");
                Log.d("RESPONSE", response);

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for(int i=0; i < jsonArray.length(); i++) {
                        JSONObject jsonobject = jsonArray.getJSONObject(i);
                        String valueResponse = jsonobject.getString("valueResponse");
                        Log.d("RESPONSE", valueResponse);

                        DownloadManager.Request dmr = new DownloadManager.Request(Uri.parse(valueResponse));
                        String fileName = URLUtil.guessFileName(valueResponse, null, MimeTypeMap.getFileExtensionFromUrl(valueResponse));
                        dmr.setTitle(fileName);
                        dmr.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
                        dmr.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                        dmr.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                        DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                        manager.enqueue(dmr);

                        Log.d("RESPONSE", fileName);

                        String zipFile = (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)) + "/" + fileName;  //your zip file location
                        String unzipLocation = String.valueOf((Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS))); // destination folder location

                        AsyncDownload df= new AsyncDownload(zipFile, unzipLocation);
                        df.unzip();

                        File file = new File(Environment.getExternalStorageDirectory(),"DocumentscargaInicial_2019-03-15_12984.txt");
                        StringBuilder text = new StringBuilder();
                        try {
                            BufferedReader br = new BufferedReader(new FileReader(file));
                            String line;
                            while ((line = br.readLine()) != null) {
                                text.append(line);
                                text.append('\n');
                            }
                        }
                        catch (IOException e) {
                            Log.d("ERROR", "error");
                            e.printStackTrace();
                        }

                        try {
                            JSONObject obj = new JSONObject(text.toString());
                            JSONArray dataArray = obj.getJSONArray("data");
                            Log.d("data", "data");

                            for (int d = 0; d < dataArray.length(); d++){
                                JSONObject dataArrayJSONObject = dataArray.getJSONObject(d);
                                JSONArray ubicacionesArray = dataArrayJSONObject.getJSONArray("UBICACIONES");
                                Log.d("UBICACIONES", "UBICACIONES");

                                for (int u = 0; u < ubicacionesArray.length(); u++){
                                    JSONObject ubicacionesArrayJSONObject = ubicacionesArray.getJSONObject(u);
                                    JSONArray proyectosArray = ubicacionesArrayJSONObject.getJSONArray("PROYECTOS");
                                    Log.d("PROYECTOS", "PROYECTOS");

                                    Double FNLATITUD = ubicacionesArrayJSONObject.getDouble("FNLATITUD");
                                    Double FNLONGITUD = ubicacionesArrayJSONObject.getDouble("FNLONGITUD");

                                    Log.d("FNLATITUD", FNLATITUD.toString());
                                    Log.d("FNLONGITUD", FNLONGITUD.toString());

                                    LatLng tmpLatLng = new LatLng(FNLATITUD, FNLONGITUD);
                                    googleMap.addMarker(new MarkerOptions().position(tmpLatLng));

                                    CameraPosition cameraPosition = new CameraPosition.Builder().target(tmpLatLng).build();
                                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


                                    for (int m = 0; m < proyectosArray.length(); m++){
                                        JSONObject proyectosArrayJSONObject = proyectosArray.getJSONObject(m);
                                        Log.d("FNLATITUD", "FNLATITUD");

                                        JSONArray tareasArray = proyectosArrayJSONObject.getJSONArray("TAREAS");
                                        Log.d("TAREAS", "TAREAS");

                                        for (int ma = 0; ma < tareasArray.length(); ma++){
                                            JSONObject tareasArrayJSONObject = tareasArray.getJSONObject(ma);
                                            JSONArray materialesArray = tareasArrayJSONObject.getJSONArray("MATERIALES");
                                            Log.d("MATERIALES", "MATERIALES");

                                            for (int ll = 0; ll < materialesArray.length(); ll++){
                                                JSONObject materialesArrayJSONObject = materialesArray.getJSONObject(ll);

                                            }
                                        }
                                    }
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("RESPONSE", "That didnt work!");
                        Log.d("RESPONSE", error.toString());
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
//                params.put("Content-Type", "application/json");
//                params.put("Accept", "application/json");
                params.put("serviceId", "55");
                params.put("userId", "12984");
                return params;
            }
        };

        queue.add(stringRequest);
    }

    public  boolean isReadStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted1");

                return true;
            } else {

                Log.v(TAG,"Permission is revoked1");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 3);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted1");

            return true;
        }
    }

    public  boolean isWriteStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted2");
                getFile();
                return true;
            } else {

                Log.v(TAG,"Permission is revoked2");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted2");
            getFile();
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            String permissions[],
            int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_WES:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "Permission Granted");
                    getFile();
                } else {
                    Log.d(TAG, "Permission DENIED");
                }
        }
    }

    private void showExplanation(String title,
                                 String message,
                                 final String permission,
                                 final int permissionRequestCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestPermission(permission, permissionRequestCode);
                    }
                });
        builder.create().show();
    }

    private void requestPermission(String permissionName, int permissionRequestCode) {
        ActivityCompat.requestPermissions(this,
                new String[]{permissionName}, permissionRequestCode);
    }

    private void showPhoneStatePermission() {
        int permissionCheck = ContextCompat.checkSelfPermission(
                this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                showExplanation("Permission Needed", "Rationale", Manifest.permission.WRITE_EXTERNAL_STORAGE, REQUEST_PERMISSION_WES);
            } else {
                requestPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, REQUEST_PERMISSION_WES);
            }
        } else {
            Log.d(TAG, "Permission Permission (already) granted");
            getFile();
        }
    }
}
