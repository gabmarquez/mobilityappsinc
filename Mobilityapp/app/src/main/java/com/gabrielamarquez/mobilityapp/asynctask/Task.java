package com.gabrielamarquez.mobilityapp.asynctask;

import android.app.Activity;
import android.os.AsyncTask;

import com.gabrielamarquez.mobilityapp.OnTaskCompleted;

public class Task extends AsyncTask<String,String,String> {

    private final OnTaskCompleted onTaskCompleted;
    private String respuesta;

    public Task(Activity activity) {
        onTaskCompleted = (OnTaskCompleted) activity;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            int time = Integer.parseInt(params[0])*1000;

            Thread.sleep(time);
            respuesta = "Finalizo " + params[0] + " segundos";
        } catch (InterruptedException e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        return respuesta;
    }

    @Override
    protected void onPostExecute(String finish) {
        onTaskCompleted.onTaskCompleted(finish);
        super.onPostExecute(finish);
    }
}
