package com.gabrielamarquez.mobilityapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.gabrielamarquez.mobilityapp.model.Employees;

import java.util.ArrayList;
import java.util.List;

public class Database extends SQLiteOpenHelper {
    public static final String DBNAME = "employees.db";
    public static final String DBLOCATION = "/data/data/com.gabrielamarquez.mobilityapp/databases/";
    private Context mContext;
    private SQLiteDatabase mDatabase;

    public Database(Context context) {
        super(context, DBNAME, null, 1);
        this.mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void openDatabase() {
        String dbPath = mContext.getDatabasePath(DBNAME).getPath();
        if (mDatabase != null && mDatabase.isOpen()) {
            return;
        }
        mDatabase = SQLiteDatabase.openDatabase(dbPath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    public void closeDatabase() {
        if (mDatabase != null) {
            mDatabase.close();
        }
    }

    public List<Employees> getListEmployees() {
        Employees employees = null;
        List<Employees> employeesList = new ArrayList<>();
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT * FROM Employees", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            employees = new Employees(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3));
            employeesList.add(employees);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return employeesList;
    }

    public Employees getEmployeeBId(int id) {
        Employees employees = null;
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT * FROM Employees WHERE ID = ?", new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        employees = new Employees(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3));
        //Only 1 resul
        cursor.close();
        closeDatabase();
        return employees;
    }

    public boolean updateData(int id, String nombre, String fechaNacimiento, String puesto) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id",id);
        contentValues.put("nombre",nombre);
        contentValues.put("fechaNacimiento",fechaNacimiento);
        contentValues.put("puesto",puesto);
        db.update("Employees", contentValues, "ID = ?",new String[] {String.valueOf(id)});
        return true;
    }

    public long updateEmployee(Employees employees) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("nombre", employees.getNombre());
        contentValues.put("fechaNacimiento", employees.getFechaNacimiento());
        contentValues.put("puesto", employees.getPuesto());
        String[] whereArgs = {Integer.toString(employees.getId())};
        openDatabase();
        long returnValue = mDatabase.update("Employees",contentValues, "ID=?", whereArgs);
        closeDatabase();
        return returnValue;
    }

    public long addEmployee(Employees employees) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("nombre", employees.getNombre());
        contentValues.put("fechaNacimiento", employees.getFechaNacimiento());
        contentValues.put("puesto", employees.getPuesto());
        openDatabase();
        long returnValue = mDatabase.insert("Employees", null, contentValues);
        closeDatabase();
        return returnValue;
    }
    public boolean deleteEmployeeById(int id) {
        openDatabase();
        int result = mDatabase.delete("Employees",  "ID =?", new String[]{String.valueOf(id)});
        closeDatabase();
        return result !=0;
    }
}