package com.gabrielamarquez.mobilityapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.gabrielamarquez.mobilityapp.asynctask.AsynctaskActivity;
import com.gabrielamarquez.mobilityapp.employees.EmployeesActivity;
import com.gabrielamarquez.mobilityapp.employees.ListEmployees;
import com.gabrielamarquez.mobilityapp.maps.MapsActivity;
import com.gabrielamarquez.mobilityapp.webservice.WebServiceActivity;

public class MenuActivity extends AppCompatActivity {

    Button first_point, second_point, third_point, fourth_point;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        first_point = findViewById(R.id.first_point);
        second_point = findViewById(R.id.second_point);
        third_point = findViewById(R.id.third_point);
        fourth_point = findViewById(R.id.fourth_point);

        first_point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, MapsActivity.class);
                startActivity(intent);
            }
        });

        second_point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, WebServiceActivity.class);
                startActivity(intent);
            }
        });

        third_point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, AsynctaskActivity.class);
                startActivity(intent);
            }
        });

        fourth_point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, ListEmployees.class);
                startActivity(intent);
            }
        });
    }
}
