package com.gabrielamarquez.mobilityapp.webservice;

import android.util.Log;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class AsyncDownload {
    private String zipFile;
    private String location;

    public AsyncDownload(String zipFile, String location) {
        this.zipFile = zipFile;
        this.location = location;

        _dirChecker("");
    }

    public void unzip() {
        try  {
            FileInputStream fin = new FileInputStream(zipFile);
            ZipInputStream zin = new ZipInputStream(fin);
            ZipEntry ze = null;
            while ((ze = zin.getNextEntry()) != null) {
                Log.v("Decompress", "Unzipping " + ze.getName());

                if(ze.isDirectory()) {
                    _dirChecker(ze.getName());
                } else {
                    FileOutputStream fout = new FileOutputStream(location + ze.getName());
                    BufferedOutputStream bufout = new BufferedOutputStream(fout);
                    byte[] buffer = new byte[1024];
                    int read = 0;
                    while ((read = zin.read(buffer)) != -1) {
                        bufout.write(buffer, 0, read);
                    }

                    bufout.close();

                    zin.closeEntry();
                    fout.close();
                }
            }

            zin.close();

            Log.d("Unzip", "Completado. path :  " + location);
        } catch(Exception e) {
            Log.e("Decompress", "unzip", e);

            Log.d("Unzip", "Unzipping fallo");
        }

    }

    private void _dirChecker(String dir) {
        File f = new File(location + dir);

        if(!f.isDirectory()) {
            f.mkdirs();
        }
    }


}
