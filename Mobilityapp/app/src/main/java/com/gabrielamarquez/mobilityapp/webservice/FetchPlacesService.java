package com.gabrielamarquez.mobilityapp.webservice;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.gabrielamarquez.mobilityapp.R;
import com.gabrielamarquez.mobilityapp.model.Place;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.ArrayList;

public class FetchPlacesService extends IntentService {

    public static String NOTIFICATION = "maps";
    public static String RESULT = "dataResult";

    java.net.URL jsonURL;
    String jsonResult="";

    public FetchPlacesService() {
        super("fetchplaces");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        publishData( fetchData() );
    }
    private ArrayList<Place> fetchData() {

        String url = getResources().getString(R.string.places_url);
        ArrayList<Place> result = new ArrayList();

        try {
            Log.d("SALIDA","si se entra");

            jsonURL = new java.net.URL(url);
            HttpURLConnection conn = (HttpURLConnection) jsonURL.openConnection();
            conn.setReadTimeout(15001);
            conn.setConnectTimeout(15001);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestMethod("GET");
            conn.connect();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(jsonURL.openStream()));

            String tmpResult="";
            while ((tmpResult = bufferedReader.readLine()) != null ) {
                jsonResult +=tmpResult;
            }

            bufferedReader.close();

            try {

                JSONObject jsonRootObject = new JSONObject(jsonResult);
                JSONObject jsonPlacesObject = jsonRootObject.getJSONObject("places");
                JSONArray jsonArray = jsonPlacesObject.getJSONArray("placesList");

                for(int i=0; i<= jsonArray.length(); i++){

                    JSONObject tmpObject = jsonArray.getJSONObject(i);

                    Place tmp = new Place("",0,0);

                    tmp.setId(tmpObject.getInt("placeId"));
                    tmp.setPlaceName(tmpObject.getString("placeName"));
                    tmp.setLat(tmpObject.getDouble("latitude"));
                    tmp.setLon(tmpObject.getDouble("longitude"));
                    result.add(tmp);
                }

            } catch (JSONException e){
                e.printStackTrace();
            }
        } catch (IOException e){
            e.printStackTrace();
        }

        return result;
    }

    public void publishData(ArrayList<Place> result){

        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(RESULT, result);
        sendBroadcast(intent);
    }
}
