package com.gabrielamarquez.mobilityapp.employees;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.gabrielamarquez.mobilityapp.R;
import com.gabrielamarquez.mobilityapp.database.Database;
import com.gabrielamarquez.mobilityapp.model.Employees;

import java.util.Calendar;


public class EmployeesActivity extends Activity implements DatePickerDialog.OnDateSetListener {

    private TextView titleEmp;
    private EditText nameEmp;
    private EditText birthdate;
    private EditText jomEmp;
    private Button btnSave;
    private Database dbHelper;
    private String action;
    private ImageButton showDialog;

    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private static final String TAG = "Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_employees);

        titleEmp = (TextView) findViewById(R.id.titleEmp);

        nameEmp = (EditText) findViewById(R.id.nameEmp);
        birthdate = (EditText) findViewById(R.id.birthdate);
        jomEmp = (EditText) findViewById(R.id.jobEmp);

        showDialog = (ImageButton)findViewById(R.id.show_dialog);

        showDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                Log.d(TAG, "onDateSet: mm/dd/yyy: " + month + "/" + day + "/" + year);

                String date = month + "/" + day + "/" + year;
                birthdate.setText(date);
            }
        };

        btnSave = (Button) findViewById(R.id.saveButton);

        action = getIntent().getExtras().getString("Action");
        titleEmp.setText(action);
        btnSave.setText(action);
        dbHelper = new Database(getApplicationContext());

        if ("Edit".equals(action)) {

            int idEmp = getIntent().getExtras().getInt("Id");
            Employees employees = dbHelper.getEmployeeBId(idEmp);

            nameEmp.setText(employees.getNombre());
            birthdate.setText(employees.getFechaNacimiento());
            jomEmp.setText(employees.getPuesto());
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Validacion
                if (nameEmp.getText().toString().isEmpty()) {
                    Toast.makeText(getBaseContext(), "Ingresa nombre", Toast.LENGTH_SHORT).show();
                } else if (birthdate.getText().toString().isEmpty()) {
                    Toast.makeText(getBaseContext(), "Ingresa fecha nacimiente", Toast.LENGTH_SHORT).show();
                } else if (jomEmp.getText().toString().isEmpty()) {
                    Toast.makeText(getBaseContext(), "Ingresa puesto", Toast.LENGTH_SHORT).show();
                } else {

                    Employees employees = new Employees(nameEmp.getText().toString(), birthdate.getText().toString(), jomEmp.getText().toString());

                    if ("Edit".equals(action)) {
                        int idEmp = getIntent().getExtras().getInt("Id");

                        boolean isUpdate = dbHelper.updateData(idEmp, nameEmp.getText().toString(), birthdate.getText().toString(), jomEmp.getText().toString());
                        if (isUpdate == true) {
                            Toast.makeText(EmployeesActivity.this, "Actulizado", Toast.LENGTH_LONG).show();
                            finish();

                        } else {
                            Toast.makeText(EmployeesActivity.this, "fllido", Toast.LENGTH_LONG).show();
                        }

                    } else {

                        long result = dbHelper.addEmployee(employees);
                        if (result > 0) {

                            Toast.makeText(getApplicationContext(), "Agregado", Toast.LENGTH_SHORT).show();
                            //back to main activity
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), "fallido", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

            }
        });

    }

    public void showDatePickerDialog(){
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                EmployeesActivity.this,
                EmployeesActivity.this,
                Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String date = month + "/" + dayOfMonth + "/" + year;
        birthdate.setText(date);
    }

}
