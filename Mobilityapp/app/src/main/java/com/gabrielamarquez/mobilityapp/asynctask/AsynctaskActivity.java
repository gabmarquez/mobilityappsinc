package com.gabrielamarquez.mobilityapp.asynctask;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.gabrielamarquez.mobilityapp.OnTaskCompleted;
import com.gabrielamarquez.mobilityapp.R;

public class AsynctaskActivity extends AppCompatActivity implements OnTaskCompleted {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asynctask);

        final Task task = new Task(this);

        LinearLayout linearLayout = findViewById(R.id.ll);

        final Button buttonCode = new Button(this);
        buttonCode.setText(R.string.button_txt);
        buttonCode.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        buttonCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sleepTime = "10";
                task.execute(sleepTime);
                buttonCode.setEnabled(false);
            }
        });

        // Add Button to LinearLayout
        if (linearLayout != null) {
            linearLayout.addView(buttonCode);
        }

    }

    @Override
    public void onTaskCompleted(String finish) {
        Toast.makeText(getApplicationContext(), "Ha finalizado", Toast.LENGTH_LONG).show();
        Toast.makeText(getApplicationContext(), "Asynctask solo puede ejecutarse una vez", Toast.LENGTH_LONG).show();
    }
}
